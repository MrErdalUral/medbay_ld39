﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;

public class DeathKeeper : MonoBehaviour
{

    public float GameLength = 300;
    public List<Death> DeathList;
    private GameObject _activeDeathPanel;
    public GameObject LastPanel;
    private int _index;
    private bool _gameEnded;
    private int _deathCount;
    public ImageController JamesImage;
    public ImageController JennyImage;
    public ImageController MaryImage;
    public ImageController PrinceImage;
    public ImageController SusanImage;
    public ImageController YorickImage;
    public Text ScoreText;
    public Text TimeText;
    
    // Use this for initialization
    void Awake()
    {
        DeathList = new List<Death>();
        _index = 0;
    }

    void Update()
    {
        var time = GameLength - Time.timeSinceLevelLoad;
        var t = TimeSpan.FromSeconds(time);

        TimeText.text = string.Format("{0:D2}:{1:D2}",
            t.Minutes,
            t.Seconds);
        if (Time.timeSinceLevelLoad > GameLength && !_gameEnded)
            DisplayEndGameUI();

        if (ScoreText != null)
        {
            var grade = string.Empty;
            switch (_deathCount)
            {
                case 0:
                    grade = "<color='#4ef442'>Perfect</color>";
                    break;
                case 1:
                    grade = "<color='#92f441'>Excellent</color>";
                    break;
                case 2:
                    grade = "<color='#b5f441'>Good</color>";
                    break;
                case 3:
                    grade = "<color='#e5f441'>Average</color>";
                    break;
                case 4:
                    grade = "<color='#f4d641'>Bad</color>";
                    break;
                case 5:
                    grade = "<color='#f47641'>Terrible</color>";
                    break;
                case 6:
                    grade = "<color='#ff1c1c'>Failed Hard</color>";
                    break;
            }
            ScoreText.text =
            (string.Format("Game Over{0}{0}You kept {1} out of 6 patients alive{0} Your Grade is: {2}", Environment.NewLine,
                6 - _deathCount, grade));

        }
    }
    public void DisplayEndGameUI()
    {
        AudioManager.Instance.StopAll();
        
        TimeText.gameObject.SetActive(false);
        _gameEnded = true;
        Time.timeScale = 0f;
        NextItem();
    }

    public void NextItem()
    {
        if (_index >= DeathList.Count)
        {
            LastPanel.SetActive(true);
        }
        if (_activeDeathPanel != null)
        {
            Destroy(_activeDeathPanel.gameObject);
        }
        if (_index < DeathList.Count)
        {
            _activeDeathPanel = Instantiate(DeathList[_index].DeathPanel);
            _activeDeathPanel.GetComponent<TextController>().DeathTime = DeathList[_index].DeathTime;
            _activeDeathPanel.transform.SetParent(transform, false);
            _index++;
        }
    }

    public void AddDeath(GameObject deathPanel)
    {
        if (_gameEnded) return;
        if (DeathList.Select(m => m.DeathPanel.name).Any(m => m == deathPanel.name)) return;
        Debug.Log(deathPanel.name + "Added");
        switch (deathPanel.name)
        {
            case "JamesDeathPanel":
                JamesImage.Index = 1;
                break;
            case "JennyDeathPanel":
                JennyImage.Index = 1;
                break;
            case "MaryDeathPanel":
                MaryImage.Index = 1;
                break;
            case "PrinceAlphonsoDeathPanel":
                PrinceImage.Index = 1;
                break;
            case "SusanDeathPanel":
                SusanImage.Index = 1;
                break;
            case "YorickDeathPanel":
                YorickImage.Index = 1;
                break;
        }
        DeathList.Add(new Death { DeathTime = Time.timeSinceLevelLoad, DeathPanel = deathPanel });
        _deathCount = DeathList.Count;
    }

    public void ReturnToMenu()
    {
        SceneManager.LoadScene("menu");
    }

    public void AddNurseDeath(GameObject deathPanel)
    {
        if (_gameEnded) return;
        if (DeathList.Select(m => m.DeathPanel.name).Any(m => m == deathPanel.name)) return;
        Debug.Log(deathPanel.name + "Added");
        switch (deathPanel.name)
        {
            case "JamesDeathPanel":
                JamesImage.Index = 1;
                break;
            case "JennyDeathPanel":
                JennyImage.Index = 1;
                break;
            case "MaryDeathPanel":
                MaryImage.Index = 1;
                break;
            case "PrinceAlphonsoDeathPanel":
                PrinceImage.Index = 1;
                break;
            case "SusanDeathPanel":
                SusanImage.Index = 1;
                break;
            case "YorickDeathPanel":
                YorickImage.Index = 1;
                break;
        }
        DeathList.Add(new Death { DeathTime = Time.timeSinceLevelLoad, DeathPanel = deathPanel });
        _deathCount = 6;
    }
}

public class Death
{
    public GameObject DeathPanel;
    public float DeathTime;
}
