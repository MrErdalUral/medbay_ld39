using System.Collections;
using UnityEngine;

public class HealthScreen : MonoBehaviour
{
    public Patient Patient;
    public GameObject RoomShadow;
    public bool Powered;
    public float DrainRate;

    public Sprite[] Sprites;

    private int _status;
    private int _health;
    private SpriteRenderer _renderer;

    private PowerSupply _powerSupply;

    /// <summary>
    /// 0 -> Green
    /// 1 -> Yellow
    /// 2 -> Red
    /// </summary>
    public int Status
    {
        get
        {

            return _status;
        }
        set
        {
            _status = value;
            if (_renderer != null)
            {
                var index = _status * 3 + _health - 1;
                if (index < 0)
                {
                    index = 9;
                }
                _renderer.sprite = (Sprites[index < Sprites.Length ? index : Sprites.Length - 1]);
            }
        }
    }
    /// <summary>
    /// A value from 1 to 3
    /// </summary>
    public int Health
    {
        get { return _health; }
        set
        {
            _health = value;
            if (_renderer != null)
            {
                var index = _status * 3 + _health - 1;
                _renderer.sprite = (Sprites[index < Sprites.Length ? index : Sprites.Length - 1]);
            }

        }
    }

    void Awake()
    {
        _renderer = GetComponent<SpriteRenderer>();
    }

    private void Start()
    {
        if (Patient && !Patient.HealthScreen)
        {
            Patient.HealthScreen = this;
        }

        _powerSupply = GameManager.Instance.PowerSupply;
    }

    // Update is called once per frame
    void Update()
    {
        if (_powerSupply.AvailablePower.Approx0() && Powered)
        {
            Powered = !Powered;
            _powerSupply.RoomDrainRate += Powered ? DrainRate : -DrainRate;
        }

        if (!Patient)
        {
            _status = 0;
            _health = 1;
            return;
        }

        if (Patient.HealthScreen.Powered)
        {
            Status = 0;
        }
        else if (Patient.HealthDrainRate.Approx0())
        {
            Status = 1;
        }
        else
        {
            Status = 2;
        }

        Health = Mathf.CeilToInt(Patient.Health / 10);
        if (Patient.IsDead)
        {
            _renderer.sprite = Sprites[9];
            if (Powered)
                TogglePower();
        }
        if (RoomShadow != null)
            RoomShadow.SetActive(!Powered);
    }

    public void TogglePower()
    {
        if (_powerSupply.AvailablePower.Approx0())
            return;
        if (Patient.IsDead)
            return;

        Powered = !Powered;

        _powerSupply.RoomDrainRate += Powered ? DrainRate : -DrainRate;
    }
}
