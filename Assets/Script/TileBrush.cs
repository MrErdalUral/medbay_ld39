﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileBrush : MonoBehaviour
{

    public Vector2 BrushSize = Vector2.zero;
    public int TileId = 0;
    public SpriteRenderer Renderer2D;

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(transform.position, BrushSize);
    }

    public void UpdateBrush(Sprite sprite)
    {
        Renderer2D.sprite = sprite;
    }
}
