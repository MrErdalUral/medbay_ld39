using UnityEngine;

public class RoomManager : MonoBehaviour
{
    public HealthScreen[] HealthScreens;

    private PowerSupply _powerSupply;
    
    private void Start()
    {
        _powerSupply = GameManager.Instance.PowerSupply;
    }
    
    private void Update()
    {
        var drainRate = 0f;
        for (var i = HealthScreens.Length - 1; i >= 0; i--)
        {
            var room = HealthScreens[i];
            if (room.Powered)
            {
                drainRate += room.Patient.PowerDrainRate;
            }
        }

        _powerSupply.RoomDrainRate = drainRate;
    }
}
