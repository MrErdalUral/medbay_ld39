﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class ZombieSpawner : MonoBehaviour
{
    public float MaximumSpawnTime = 20f;
    public float MinimumSpawnTime = 10f;
    public float CurrentSpawnTime;
    public GameObject ZombiePrefab;
    private float _spawnTimer;
	// Use this for initialization
	void Start ()
	{
	    CurrentSpawnTime = MaximumSpawnTime;
	}
	
	// Update is called once per frame
	void Update ()
	{
	    _spawnTimer += Time.deltaTime;
	    if (_spawnTimer > CurrentSpawnTime)
	    {
	        _spawnTimer = 0;
	        Instantiate(ZombiePrefab, transform.position, Quaternion.identity);
	        CurrentSpawnTime = Random.Range(MinimumSpawnTime, MaximumSpawnTime);
	    }
	}
}
