using UnityEngine;

public class AudioPlayerLight : MonoBehaviour
{
    public AudioClip Clip;
    
    public float Volume = 1;
    public float MaxPitchOffset;
    public float Delay;
    
    public bool Loop;
    public bool AffectedByDistance;

    public bool PlayInstantly;
    
    public float MinDistance; // volume will start to decrease when the distance is greater than MinDistance
    public float MaxDistance; // won't be audible beyond MaxDistance

    public bool IsPlaying { get { return _audioSource && _audioSource.isPlaying; } }
    
    private Transform _player;

    private AudioSource _audioSource;
    
    private void Start()
    {
        _player = GameManager.Instance.Player.transform;
        if (PlayInstantly)
        {
            _audioSource = AudioManager.Instance.AvailableSource;
            Play();
        }
    }

    private void Update()
    {
        if (Loop && _audioSource)
            UpdateAudio();
    }

    private void UpdateAudio()
    {
        _audioSource.clip = Clip;
        _audioSource.loop = Loop;
        
        if (AffectedByDistance)
        {
            var distance = (_player.position - transform.position).magnitude;
        
            if (distance < MinDistance)
                Volume = 1;
            else if (distance > MaxDistance)
                Volume = 0;
            else
                Volume = Mathf.Lerp(1, 0, distance / (MaxDistance - MinDistance));

            _audioSource.volume = Volume;
        }
        
        if (MaxPitchOffset > 0)
            _audioSource.RandomizePitch(MaxPitchOffset);
    }

    public void Play()
    {
        if (_audioSource)
        {
            UpdateAudio();
            _audioSource.Play(Clip.Delay(Delay));
        }
        else
            _audioSource = AudioManager.Instance.Play(Clip, Volume, Loop, MaxPitchOffset, Delay);
    }

    public void Stop()
    {
        if (_audioSource)
        {
            _audioSource.Stop();
            
            if (PlayInstantly)
                AudioManager.Instance.Release(_audioSource);
            
            _audioSource = null;
        }
    }
}
