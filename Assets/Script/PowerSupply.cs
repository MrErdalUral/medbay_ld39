﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerSupply : MonoBehaviour
{

    public float PowerCapacity;
    public float AvailablePower;

    [SerializeField]
    private float _baseDrainRate;
    public float BaseDrainRate
    {
        get { return _baseDrainRate; }
        private set { _baseDrainRate = value; }
    }

    public float RoomDrainRate;

    public Slider EnergyBar;

    private Image _barBackground;
    private Image _barForeground;

    public Color32 HighLevelFore;
    public Color32 HighLevelBack;
    public Color32 MediumLevelFore;
    public Color32 MediumLevelBack;
    public Color32 LowLevelFore;
    public Color32 LowLevelBack;

    public float HighLevelMinRatio;
    public float MediumLevelMinRatio;

    private void Start()
    {
        if (AvailablePower.Approx0())
        {
            AvailablePower = PowerCapacity;
        }

        EnergyBar.maxValue = PowerCapacity;

        _barBackground = EnergyBar.transform.GetChild(0).GetComponent<Image>();
        _barForeground = EnergyBar.transform.GetChild(1).GetChild(0).GetComponent<Image>();
    }

    private void Update()
    {
        AvailablePower -= (BaseDrainRate + RoomDrainRate) * Time.deltaTime;
        AvailablePower = Mathf.Clamp(AvailablePower, 0, PowerCapacity);

        EnergyBar.value = AvailablePower;

        UpdatePowerLevelColors();

        if (Input.GetKeyDown(KeyCode.P))
        {
            StartCoroutine(AddTempPower(50, 3));
        }
    }

    private void UpdatePowerLevelColors()
    {
        var ratio = AvailablePower / PowerCapacity;
        if (ratio > HighLevelMinRatio)
        {
            _barBackground.color = HighLevelBack;
            _barForeground.color = HighLevelFore;
        }
        else if (ratio > MediumLevelMinRatio)
        {
            _barBackground.color = MediumLevelBack;
            _barForeground.color = MediumLevelFore;
        }
        else
        {
            _barBackground.color = LowLevelBack;
            _barForeground.color = LowLevelFore;
        }
    }

    public void AddPower(float power)
    {
        var player = GameObject.FindObjectOfType<PlayerMovement>();
        if (player == null) return;
        if (player.Zombie == null) return;
        Destroy(player.Zombie);
        player.Zombie = null;
        AvailablePower += power;
    }

    public IEnumerator AddTempPower(float power, float duration)
    {
        AvailablePower += power;
        print(string.Format("Added {0} power for {1} s", power, duration));
        yield return new WaitForSeconds(duration);

        print(string.Format("Removing {0} power. {1} s is up!", power, duration));
        AvailablePower -= power;
    }

    public IEnumerator AddTempPowerDrain(float rate, float duration)
    {
        BaseDrainRate += rate;

        print(string.Format("Added {0} drain for {1} s", rate, duration));

        yield return new WaitForSeconds(duration);

        print(string.Format("Removing {0} drain. {1} s is up!", rate, duration));

        BaseDrainRate -= rate;
    }

    public IEnumerator StopPowerDrainTemp(float duration)
    {
        var drainRate = BaseDrainRate;
        BaseDrainRate = 0;

        print(string.Format("Stopping power drain for {0} s.", duration));

        yield return new WaitForSeconds(duration);

        print("Continuing power drain.");
        BaseDrainRate = drainRate;
    }
}
