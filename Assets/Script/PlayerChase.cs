﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerChase : MonoBehaviour
{

    public Vector2 XMovementRange = new Vector2();
    public Vector2 YMovementRange = new Vector2();

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        var player = GameManager.Instance.Player;

        if (player != null)
        {
            if (player.transform.position.x > XMovementRange.x && player.transform.position.x < XMovementRange.y)
                transform.position = new Vector3(player.transform.position.x, transform.position.y, transform.position.z);
            if (player.transform.position.y > YMovementRange.x && player.transform.position.y < YMovementRange.y)
                transform.position = new Vector3(transform.position.x, player.transform.position.y, transform.position.z);
        }
        else
        {
            Debug.Log("Cannot get player reference");
        }
    }
}
