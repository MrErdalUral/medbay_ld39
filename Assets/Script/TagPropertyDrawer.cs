using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
[CustomPropertyDrawer(typeof(TagAttribute))]

public class TagPropertyDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {

        EditorGUI.BeginProperty(position, label, property);

        var tags = UnityEditorInternal.InternalEditorUtility.tags;
        var tag = property.stringValue;
        var index = 0;

        for (var i = 0; i < tags.Length; i++)
        {
            if (tags[i] == tag)
            {
                index = i;
                break;
            }
        }

        index = EditorGUI.Popup(position, label.text, index, tags);
        property.stringValue = tags[index];

        EditorGUI.EndProperty();
    }
}
#endif
