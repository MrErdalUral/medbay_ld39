using System.Collections;
using UnityEngine;

public class PatientDie : MonoBehaviour
{
    public float FadeDuration;
    public float GhostDuration;

    public float FloatMultiplier;
    public float OscillationMultiplier;
    
    private Patient _patient;

    private SpriteRenderer _spriteRendererDead;
    private SpriteRenderer _spriteRendererGhost;

    private Transform _ghost;
    
    private bool _changedSprite;
    private bool _ghostDisappeared;
    
    private float _startTime;

    private float _t;

    private void Awake()
    {
        _patient = GetComponent<Patient>();
        _ghost = transform.FindChild("Ghost");
        
        _spriteRendererDead = transform.FindChild("Corpse").GetComponent<SpriteRenderer>();
        _spriteRendererGhost = _ghost.GetComponent<SpriteRenderer>();

        _spriteRendererDead.color = new Color(1, 1, 1, 0);
        _spriteRendererGhost.color = new Color(1, 1, 1, 0);
    }
    
    // todo maybe do the fading without polling
    private void Update()
    {
        if (_patient.IsDead)
        {
            if (_startTime.Approx0())
            {
                _startTime = Time.time;
            }

            var t = Time.time - _startTime;

            if (!_changedSprite)
                FadeToDead(t);
            if (!_ghostDisappeared)
            {
                StartCoroutine(SetGhostDisappeared());
                AnimateGhost(t);
            }
        }
    }

    private void FadeToDead(float timeDifference)
    {
        var t = timeDifference / FadeDuration;

        var step = Mathf.SmoothStep(0, 1, t);
        _spriteRendererDead.color = new Color(1, 1, 1, step);

        if (_spriteRendererDead.color.a.Approx(1))
        {
            _changedSprite = true;
        }
    }

    private void AnimateGhost(float timeDifference)
    {
        var t = timeDifference / GhostDuration;

        float alpha;
        if (t < 0.25)
            alpha = Mathf.Lerp(0, 1, 4 * t);
        else if (t > 0.25 && t < 0.75)
            alpha = 1;
        else
            alpha = Mathf.Lerp(1, 0, 4 * (t - 0.75f));
        
        _spriteRendererGhost.color = new Color(1, 1, 1, alpha);

        var z = Mathf.Cos(OscillationMultiplier * (1 - t)) * 45 + 270; // 225 <-> 315

        _ghost.localRotation = Quaternion.Euler(0, 0, z);
        
        _ghost.Translate(FloatMultiplier * (1 - t) * Vector3.up * Time.deltaTime);
    }

    private IEnumerator SetGhostDisappeared()
    {
        yield return new WaitForSeconds(GhostDuration);
        _ghostDisappeared = true;
    }
}
