﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextController : MonoBehaviour
{
    public Text Text;
    public float DeathTime;
    public string OriginalText;

    void Awake()
    {
        Text = GetComponentInChildren<Text>();
        OriginalText = Text.text;
    }

    void Update()
    {
        var t = TimeSpan.FromSeconds(DeathTime);
        
        Text.text = OriginalText.Replace("(game time)", string.Format("{0:D2}:{1:D2}",
                t.Minutes,
                t.Seconds));
    }

    public void Next()
    {
        GameObject.FindObjectOfType<DeathKeeper>().NextItem();
    }
}
