using System;
using System.Collections;
using UnityEngine;

public class Patient : MonoBehaviour
{
    public DateTime Birthdate;
    public GameObject DeathPanel;
    public int Age
    {
        get { return Convert.ToInt32((GameManager.Instance.CurrentDateTime - Birthdate).TotalDays / 365.25); }
    }
    
    public string Name;
    public string Condition;

    public string DeathText;
    public string SurvivalText;

    public float PowerDrainRate;

    public float Health;
    public float MaxHealth;
    public float HealthDrainRate;
    public float HealthGainRate;

    public bool IsAlive { get { return Health > 0; }}
    public bool IsDead { get { return !IsAlive; }}

    public HealthScreen HealthScreen;

    private bool _healthDrainPaused;

    private void Start()
    {
        if (HealthScreen && !HealthScreen.Patient)
        {
            HealthScreen.Patient = this;
        }
    }

    private void Update()
    {
        if (IsDead)
            return;
        
        if (HealthScreen.Powered)
            Health += HealthGainRate * Time.deltaTime;
        if (!HealthScreen.Powered)
            Health -= HealthDrainRate * Time.deltaTime;

        Health = Mathf.Clamp(Health, 0, MaxHealth);
        if (Health.Approx0())
        {
            var keeper = GameObject.FindObjectOfType<DeathKeeper>();
            keeper.AddDeath(DeathPanel);
        }
    }

    public void StopHealthDrainTemp(float duration)
    {
        if (!_healthDrainPaused)
        {
            StartCoroutine(StopHealthDrainTempCoroutine(duration));
        }
    }
    
    private IEnumerator StopHealthDrainTempCoroutine(float duration)
    {
        var drainRate = HealthDrainRate;
        HealthDrainRate = 0;
        
        _healthDrainPaused = true;
        
        print(string.Format("Paused health drain for {0}", duration));
        
        yield return new WaitForSeconds(duration);

        HealthDrainRate = drainRate;

        _healthDrainPaused = false;
        print(string.Format("Continuing health drain for {0}", duration));
    }

    public void Kill()
    {
        print(string.Format("a zombie killed {0}", name));
        Health = 0;
        var keeper = GameObject.FindObjectOfType<DeathKeeper>();
        keeper.AddDeath(DeathPanel);
    }
}
