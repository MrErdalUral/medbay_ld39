using System;
using UnityEngine;

public static class Extensions
{
    public static bool Approx(this float f, float other) { return Mathf.Approximately(f, other); }
    public static bool Approx(this float f, float other, float diff) { return Mathf.Abs(f - other) < diff; }
    public static bool Approx0(this float f) { return f.Approx(0); }
    
    public static Vector2 xy(this Vector3 v) { return new Vector2(v.x, v.y); }

    public static Vector3 To3d(this Vector2 v, float z = 0)
    {
        return new Vector3(v.x, v.y, z);
    }

    public static bool Has(this PlayerMovement.Direction d, PlayerMovement.Direction other)
    {
        return (d & other) == other;
    }

    public static PlayerMovement.Direction Remove(this PlayerMovement.Direction d, PlayerMovement.Direction other)
    {
        return d &= ~other;
    }

    public static PlayerMovement.Direction Add(this PlayerMovement.Direction d, PlayerMovement.Direction other)
    {
        d |= other;

        if (d.Has(PlayerMovement.Direction.Up) && d.Has(PlayerMovement.Direction.Down))
        {
            d = d.Remove(PlayerMovement.Direction.Up).Remove(PlayerMovement.Direction.Down);
        }

        if (d.Has(PlayerMovement.Direction.Left) && d.Has(PlayerMovement.Direction.Right))
        {
            d = d.Remove(PlayerMovement.Direction.Left).Remove(PlayerMovement.Direction.Right);
        }

        return d;
    }

    public static PlayerMovement.Direction ToDirection(this Vector2 v)
    {
        var d = PlayerMovement.Direction.None;

        if (v.x.Approx(1))
            d |= PlayerMovement.Direction.Right;
        else if (v.x.Approx(-1))
            d |= PlayerMovement.Direction.Left;

        if (v.y.Approx(1))
            d |= PlayerMovement.Direction.Up;
        else if (v.y.Approx(-1))
            d |= PlayerMovement.Direction.Down;

        return d;
    }

    public static Vector2 ToVector(this PlayerMovement.Direction d)
    {
        var x =
            d.Has(PlayerMovement.Direction.Left) ? -1 :
            d.Has(PlayerMovement.Direction.Right) ? 1 :
            0;

        var y =
            d.Has(PlayerMovement.Direction.Down) ? -1 :
            d.Has(PlayerMovement.Direction.Up) ? 1 :
            0;

        return new Vector2(x, y);
    }

    public static bool TagExistsIn(this GameObject go, string[] strings)
    {
        for (var i = strings.Length - 1; i >= 0; i--)
        {
            if (go.CompareTag(strings[i]))
            {
                return true;
            }
        }
        return false;
    }

    public static ulong Delay(this AudioClip clip, float seconds)
    {
        return Convert.ToUInt64(Mathf.Ceil(clip.samples * seconds));
    }

    public static AudioSource RandomizePitch(this AudioSource source, float maxOffset)
    {
        source.pitch = 1 + UnityEngine.Random.Range(0, maxOffset);
        return source;
    }

    public static float Random(this Vector2 v) { return UnityEngine.Random.Range(v.x, v.y); }
}
