﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{

    public Sprite DoorOpenSprite;
    public Sprite DoorClosedSprite;
    private SpriteRenderer _renderer;

    void Awake()
    {
        _renderer = GetComponent<SpriteRenderer>();
    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag!="Player" && collision.tag != "Zombie") return;
        if(DoorOpenSprite == null) return;
        _renderer.sprite = DoorOpenSprite;
    }
    public void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag != "Player" && collision.tag != "Zombie") return;
        if (DoorClosedSprite == null) return;
        _renderer.sprite = DoorClosedSprite;

    }
}
