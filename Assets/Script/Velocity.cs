﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Velocity : MonoBehaviour
{
    public float Speed;
    private Rigidbody2D _rigidbody;
    public float Variance = 1f;
    public float _variance;

    private PlayerMovement _playerMovement;
    private Vector2 _direction;

    // Use this for initialization
    void Awake()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        _variance = Random.Range(-Variance, Variance);
        Destroy(gameObject, .5f);

        _playerMovement = GameManager.Instance.Player.GetComponent<PlayerMovement>();
        _direction = _playerMovement.LastMovementDirection.ToVector();
    }

    void FixedUpdate()
    {
        var d = _direction.normalized * Speed;
        var dperp = (Quaternion.Euler(0, 0, -90) * d.normalized * _variance).xy();

        _rigidbody.velocity = d + dperp;
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "Zombie")
        {
            collision.transform.GetComponent<Zombie>().Death();
        }
        Destroy(gameObject);
    }
    
}
