﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float MovementSpeed = 32f;
    private Rigidbody2D _rigidbody;
    private Animator _animator;
    public GameObject Zombie;

    public Direction LastMovementDirection = Direction.None;

    private AudioPlayerLight _audioPlayer;
    
    // Use this for initialization
    void Awake()
    {
        _animator = GetComponent<Animator>();
        _rigidbody = GetComponent<Rigidbody2D>();
        _audioPlayer = GetComponent<AudioPlayerLight>();
    }

    // Update is called once per frame
    void Update()
    {

        var inputVector = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        // if (Input.GetButton("Jump"))
        //     inputVector = Vector2.zero;
        if (inputVector == Vector2.zero)
        {
            _animator.SetBool("Walking", false);
        }
        else
        {
            LastMovementDirection = inputVector.ToDirection();
            _animator.SetBool("Walking", true);
            transform.position += (Vector3) inputVector * MovementSpeed * Time.deltaTime;
            if (Zombie != null)
                Zombie.transform.position = transform.position + Vector3.down * 5;
        }

        if (!_audioPlayer.IsPlaying && _animator.GetBool("Walking"))
            _audioPlayer.Play();
        else if (_audioPlayer.IsPlaying && !_animator.GetBool("Walking"))
            _audioPlayer.Stop();
    }

    [Flags]
    public enum Direction
    {
        None = 0,
        Left = 1,
        Up = 1 << 1,
        Right = 1 << 2,
        Down = 1 << 3
    }
}
