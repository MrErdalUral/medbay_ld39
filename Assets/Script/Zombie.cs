﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zombie : MonoBehaviour
{
    public Sprite[] ZombiSprites;
    public float Speed = 12f;
    private ZombieNavigator _navigator;
    private Transform _targetBed;
    private bool _firstX;
    private bool _isDead;
    private Rigidbody2D _rigidbody;
    // Use this for initialization
    void Start()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        _firstX = true;
        _isDead = false;
        GetComponent<SpriteRenderer>().sprite = ZombiSprites[Random.Range(0, ZombiSprites.Length)];
    }

    // Update is called once per frame
    void Update()
    {
        if (_isDead) return;
        if (_navigator == null)
        {
            _navigator = GameObject.FindObjectOfType<ZombieNavigator>();
        }
        else
        {
            if (_targetBed == null)
            {

                if (_navigator.BedPositions != null && _navigator.BedPositions.Length > 0)
                    _targetBed = _navigator.BedPositions[Random.Range(0, _navigator.BedPositions.Length)];
            }
        }
        if (_targetBed != null)
        {
            if (_firstX)
            {

                if (Mathf.Abs(transform.position.x - _targetBed.position.x) < 1)
                {
                    if (Mathf.Abs(transform.position.y - _targetBed.position.y) < 1)
                        return;
                    var dir = Mathf.Sign(_targetBed.position.y - transform.position.y);
                    transform.position += new Vector3(0, Speed * Time.deltaTime * dir);
                }
                else
                {
                    var dir = Mathf.Sign(_targetBed.position.x - transform.position.x);
                    transform.position += new Vector3(Speed * Time.deltaTime * dir, 0);
                }
            }
            else
            {

                if (Mathf.Abs(transform.position.y - _targetBed.position.y) < 1)
                {
                    if (Mathf.Abs(transform.position.x - _targetBed.position.x) < 1)
                        return;
                    var dir = Mathf.Sign(_targetBed.position.x - transform.position.x);
                    transform.position += new Vector3(Speed * Time.deltaTime * dir, 0);
                }
                else
                {
                    var dir = Mathf.Sign(_targetBed.position.y - transform.position.y);
                    transform.position += new Vector3(0, Speed * Time.deltaTime * dir);
                }
            }
        }
    }

    public void Death()
    {
        GetComponent<Animator>().SetTrigger("Death");
        _isDead = true;
        transform.rotation = Quaternion.AngleAxis(90, new Vector3(0, 0, 1));
        gameObject.layer = LayerMask.NameToLayer("Dead");
        GetComponent<BoxCollider2D>().isTrigger = true;
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (!_isDead) return;
        if (collision.gameObject.tag == "Player")
            if (collision.gameObject.GetComponent<PlayerMovement>().Zombie == null)
            {
                collision.gameObject.GetComponent<PlayerMovement>().Zombie = gameObject;
                gameObject.layer = LayerMask.NameToLayer("NoCollision");
            }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (!_isDead) return;
        if (collision.gameObject.tag == "Player")
            if (collision.gameObject.GetComponent<PlayerMovement>().Zombie == null)
            {
                collision.gameObject.GetComponent<PlayerMovement>().Zombie = gameObject;
                gameObject.layer = LayerMask.NameToLayer("NoCollision");
            }
    }
}
