﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileMap : MonoBehaviour
{

    public Vector2 MapSize = new Vector2(20, 10);//Map size in tiles
    public Texture2D Texture2D;
    public Vector2 TileSize;//tile size in pixels
    public Object[] SpriteReferences;
    public Vector2 GridSize;//Map size in units
    public float PixelsPerUnit;
    public int TileId = 0;
    public GameObject Tiles;

    public Sprite CurrentBrushSprite
    {
        get
        {
            if (SpriteReferences == null) return null;
            return SpriteReferences[TileId] as Sprite;
        }
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnDrawGizmosSelected()
    {
        var pos = transform.position;
        if (Texture2D != null)
        {
            //draw border for every cell in tile map
            Gizmos.color = Color.gray;
            var columns = GridSize.x * PixelsPerUnit / TileSize.x;
            var rows = GridSize.y * PixelsPerUnit / TileSize.y;
            for (int i = 0; i < columns; i++)
            {
                for (int j = 0; j < rows; j++)
                {
                    var tileX = pos.x + (i + .5f) * (TileSize.x / PixelsPerUnit);
                    var tileY = pos.y + (j + .5f) * (TileSize.y / PixelsPerUnit);
                    Gizmos.DrawWireCube(new Vector2(tileX, tileY), TileSize / PixelsPerUnit);
                }
            }

            //Draw outer border with white
            Gizmos.color = Color.white;
            var centerX = pos.x + GridSize.x / 2;
            var centerY = pos.y + GridSize.y / 2;
            Gizmos.DrawWireCube(new Vector2(centerX, centerY), GridSize);

        }
    }
}
