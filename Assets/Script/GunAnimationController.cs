﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunAnimationController : MonoBehaviour
{

    private Animator _animator;
	// Use this for initialization
	void Awake ()
	{
	    _animator = GetComponent<Animator>();
	}

    public void PlayFireAnimation()
    {
        _animator.SetTrigger("Fire");
    }
}
