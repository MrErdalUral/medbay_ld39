﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;

public class PlayerGun : MonoBehaviour
{
    private int _bulletCount;
    public GameObject Gun;
    public GameObject BulletPrefab;
    public GameObject Bullet1;
    public GameObject Bullet2;

    public AudioClip FireClip;
    public AudioClip PumpClip;
    public AudioClip CasingDropClip;

    public float MaxPitchOffset;
    public Vector2 PumpDelayRange; // 0.47
    public Vector2 CasingDropDelayRange; // 1.05

    private AudioManager _audioManager;

    private PlayerMovement _playerMovement;

    void Awake()
    {
        _bulletCount = 2;
    }

    private void Start()
    {
        _audioManager = AudioManager.Instance;
        _playerMovement = GetComponent<PlayerMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            Gun.SetActive(true);
        }
        if (Input.GetButtonUp("Jump"))
        {
            if (_playerMovement.LastMovementDirection != PlayerMovement.Direction.None)
            {
                StartCoroutine(Fire());
            }
            else
            {
                Gun.SetActive(false);
            }
        }
        if (Gun.activeSelf)
        {
            var direction = _playerMovement.LastMovementDirection.ToVector();
            if (!direction.x.Approx0())
            {
                Gun.transform.localScale = new Vector3(direction.x, 1, 1);
                Gun.transform.localEulerAngles = Vector3.zero;
            }
            else
            {
                Gun.transform.localScale = Vector3.one;
                Gun.transform.localEulerAngles = new Vector3(0, 0, direction.y * 90);
            }
        }
        switch (_bulletCount)
        {
            case 2:
                Bullet1.SetActive(true);
                Bullet2.SetActive(true);
                break;
            case 1:
                Bullet1.SetActive(true);
                Bullet2.SetActive(false);
                break;
            case 0:
                Bullet1.SetActive(false);
                Bullet2.SetActive(false);
                break;
        }
    }

    private IEnumerator Fire()
    {
        if (_bulletCount > 0)
        {
            for (int i = 0; i < 4; i++)
            {
                var bullet = Instantiate(BulletPrefab, Gun.transform.position + Gun.transform.right * 12, Quaternion.identity) as GameObject;
                bullet.transform.localScale = Gun.transform.localScale;
            }
            var direction = _playerMovement.LastMovementDirection.ToVector();
            transform.position += (-direction * 10).To3d();
            Gun.GetComponent<GunAnimationController>().PlayFireAnimation();
            _audioManager.Play(FireClip, maxPitchOffset: MaxPitchOffset);
            yield return new WaitForSeconds(0.5f);
            _bulletCount--;
        }
        Gun.SetActive(false);
    }

    public void Reload()
    {
        if (_bulletCount < 2)
        {
            _audioManager.Play(CasingDropClip, maxPitchOffset: MaxPitchOffset, delay: PumpDelayRange.Random());
            _audioManager.Play(CasingDropClip, maxPitchOffset: MaxPitchOffset, delay: PumpDelayRange.Random());
            _audioManager.Play(PumpClip, maxPitchOffset: MaxPitchOffset);
        }
        _bulletCount = 2;
    }
}
