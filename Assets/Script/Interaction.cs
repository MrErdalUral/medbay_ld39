using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(BoxCollider2D))]
public class Interaction : MonoBehaviour
{
    public float InteractionTime;
    public bool ShowIndicator;
    [Tag]
    public string[] InteractionTags = { Tags.Player, Tags.Zombie };

    public UnityEvent Action;

    public UnityEvent TriggerEnter;
    public UnityEvent TriggerExit;

    private bool _enter;

    private float _collisionEnterTime;
    private PlayerActionIndicator _actionIndicator;

    private void OnTriggerEnter2D(Collider2D other)
    {
        for (var i = InteractionTags.Length - 1; i >= 0; i--)
        {
            if (!other.gameObject.TagExistsIn(InteractionTags))
            {
                return;
            }
        }
//        if (!other.gameObject.CompareTag(InteractionTags))
//            return;
        if (ShowIndicator)
            _actionIndicator = other.gameObject.GetComponent<PlayerActionIndicator>();
        _enter = true;
        TriggerEnter.Invoke();
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        for (var i = InteractionTags.Length - 1; i >= 0; i--)
        {
            if (!other.gameObject.TagExistsIn(InteractionTags))
            {
                return;
            }
        }
        _enter = false;
        if (_actionIndicator != null)
        {
            _actionIndicator.HideIndicator();
            _actionIndicator = null;
        }
        TriggerExit.Invoke();
    }

    void Update()
    {
        if (_enter)
        {
            _collisionEnterTime += Time.deltaTime;
            if (_actionIndicator != null)
                _actionIndicator.ShowActionStatus(_collisionEnterTime / InteractionTime);
        }
        else
        {
            _collisionEnterTime = 0;
            //if (_actionIndicator != null)
            //    _actionIndicator.HideIndicator();
        }
        if (_collisionEnterTime >= InteractionTime)
        {
            Action.Invoke();
            _collisionEnterTime = 0;
        }
    }
}
