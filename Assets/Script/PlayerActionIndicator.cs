﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerActionIndicator : MonoBehaviour
{

    public Slider Slider;

    private void Start()
    {
        if (!Slider)
        {
            Slider = transform.Find("Canvas/Slider").GetComponent<Slider>();
        }
    }

    public void ShowActionStatus(float perchentage)
    {
        if(!Slider.gameObject.activeSelf)
            Slider.gameObject.SetActive(true);
        Slider.value = perchentage;
    }

    public void HideIndicator()
    {
        Slider.gameObject.SetActive(false);
    }
}
