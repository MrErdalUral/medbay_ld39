﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Abandon : MonoBehaviour
{

    public GameObject NursePanel;

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            GameObject.FindObjectOfType<DeathKeeper>().AddNurseDeath(NursePanel);
            GameObject.FindObjectOfType<DeathKeeper>().GameLength = 0;
        }
    }
}
