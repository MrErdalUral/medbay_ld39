using System;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    public DateTime StartDateTime;
    public float TimeMultiplier;
    
    public DateTime CurrentDateTime
    {
        get { return StartDateTime.AddSeconds(Time.time * TimeMultiplier); }
    }

    public static GameManager Instance { get; private set; }

    public GameObject Player;

    [HideInInspector]
    public PowerSupply PowerSupply;

    [HideInInspector]
    public Patient[] Patients;

    [HideInInspector]
    public AudioManager AudioManager;

    private bool _initialized;

    private void Awake()
    {
        Instance = this;
        
        PowerSupply = FindObjectOfType<PowerSupply>();
        Patients = FindObjectsOfType<Patient>();
        Player = GameObject.FindGameObjectWithTag(Tags.Player);
        AudioManager = FindObjectOfType<AudioManager>();

        _initialized = true;
    }

    private void Start()
    {
        if (!_initialized) return;
    }
}
