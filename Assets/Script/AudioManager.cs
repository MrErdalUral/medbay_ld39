using System;
using System.Collections;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance;

    public AudioClip DefaultClip;
    public int Instances;
    
    private AudioSource[] _audioSources;
    private bool[] _availability; // if (_availability[i]) "available"

    public AudioSource AvailableSource
    {
        get
        {
            for (var i = Instances - 1; i >= 0; i--)
            {
                var audioSource = _audioSources[i];
                if (_availability[i])
                {
                    _availability[i] = false;
                    return audioSource;
                }
            }
        
            throw new Exception("No audio sources available."); 
        }
    }

    private void Awake()
    {
        Instance = this;

        _availability = new bool[Instances];

        _audioSources = new AudioSource[Instances];
        for (var i = Instances - 1; i >= 0; i--)
        {
            var audioSource = gameObject.AddComponent<AudioSource>();
            audioSource.clip = DefaultClip;
            _audioSources[i] = audioSource;
            _availability[i] = true;
        }
    }

    public AudioSource Play(AudioClip clip, float volume = 1.0f, bool loop = false, float maxPitchOffset = 0,
        float delay = 0)
    {
        var audioSource = AvailableSource;

        audioSource.clip = clip;
        audioSource.volume = volume;
        audioSource.loop = loop;
        audioSource.RandomizePitch(maxPitchOffset);

        audioSource.Play(clip.Delay(delay));
        
        Release(audioSource);

        return audioSource;
    }

    public void Release(AudioSource audioSource)
    {
        StartCoroutine(FreeAudioSource(audioSource));
    }

    private IEnumerator FreeAudioSource(AudioSource audioSource)
    {
        while (audioSource.isPlaying)
        {
            yield return new WaitForEndOfFrame();
        }
        
        for (var i = _audioSources.Length - 1; i >= 0; i--)
        {
            if (_audioSources[i] == audioSource)
            {
                _availability[i] = true;
            }
        }
    }

    public void StopAll()
    {
        for (var i = _audioSources.Length - 1; i >= 0; i--)
        {
            var audioSource = _audioSources[i];
            audioSource.Stop();
            Release(audioSource);
        }
    }
}
