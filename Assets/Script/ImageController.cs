﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageController : MonoBehaviour
{

    private Image _image;
    public int Index = 0;
    public Sprite[] Sprites;
    // Use this for initialization
    void Awake()
    {
        _image = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        _image.sprite = Sprites[Index];
    }
}
