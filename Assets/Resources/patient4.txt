Mary “With Cherry On Top” Gastronomy

Peacefully exploded on (game time) at the (hastane ve oyunun adı?) at the critical age of 29. Mary had no living relatives and no family. She was a very big person, but she loved life and more than that she loved ice cream. With cherry on top, of course.

Mary was training to become an olympic athlete when she discovered her love for ice-cream. She gave up running, took up the bowl, eventually becoming a world renowned gurme. Wherever she went, she sprinkled laughter, joy and calories. She was happy. Children loved her. Humanity loved her. She was like the product of a love affair between Mother Theresa and Santa Claus.

When the power outage tragically killed her refrigerator, Mary’s heart could not stand the blow. All her ice-cream had melted. She fell into a coma. And now she will never wake up. Children around the world will cry rivers of blood.

Our most heartfelt condolences to them and Mary.

For ice-cream will never be as tasty.