﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class TilePickerWindow : EditorWindow
{
    public enum Scale
    {
        X1,
        X2,
        X3,
        X4,
        X5
    }

    private Scale _scale;
    public Vector2 ScrollPos = Vector2.zero;
    public Vector2 CurrentSelection = Vector2.zero;

    [MenuItem("Window/TilePicker")]
    public static void OpenTilePickerWindow()
    {
        var window = EditorWindow.GetWindow(typeof(TilePickerWindow));
        var title = new GUIContent();
        title.text = "Tile Picker";
        window.titleContent = title;
    }

    void OnGUI()
    {
        if (Selection.activeGameObject == null) return;

        //Get texture properties from the gameObject selected in the scene
        var selection = Selection.activeGameObject.GetComponent<TileMap>();
        if (selection == null || selection.Texture2D == null) return;
        var texture2D = selection.Texture2D;

        //Add texture scaler GUI element and scale the texture
        _scale = (Scale)EditorGUILayout.EnumPopup("Zoom", _scale);
        var newScale = ((int)_scale) + 1;
        var newTextureSize = new Vector2(texture2D.width, texture2D.height) * newScale;
        var offSet = new Vector2(10, 25);

        //Get window properties rect properties
        var viewPortSize = new Rect(0, 0, position.width - 5, position.height - 5); // -5 for scroll bars
        var contentSize = new Rect(0, 0, newTextureSize.x + offSet.x, newTextureSize.y + offSet.y);

        //Add scrollbars
        ScrollPos = GUI.BeginScrollView(viewPortSize, ScrollPos, contentSize);
        GUI.DrawTexture(new Rect(offSet.x, offSet.y, newTextureSize.x, newTextureSize.y), texture2D);

        //Calculate the tile size and the grid size
        var tileSize = selection.TileSize * newScale; //pixels
        var gridSize = new Vector2(newTextureSize.x / tileSize.x, newTextureSize.y / tileSize.y); //tiles

        //Calculate the selected tiles position on the window
        var selectionPosition = new Vector2(tileSize.x * CurrentSelection.x + offSet.x, tileSize.y * CurrentSelection.y + offSet.y);

        //Create selection style for displaying the selected tile
        var BoxTexture = new Texture2D(1, 1);
        BoxTexture.SetPixel(0, 0, new Color(0, .7f, .1f, .4f));
        BoxTexture.Apply();

        //Draw a box with the created style on top of the selected tile
        var style = new GUIStyle(GUI.skin.customStyles[0]);
        style.normal.background = BoxTexture;
        GUI.Box(new Rect(selectionPosition.x, selectionPosition.y, tileSize.x, tileSize.y), "", style);

        //Change the selection according to the mouse event
        var currentEvent = Event.current;
        if (currentEvent.type == EventType.MouseDown && currentEvent.button == 0) //button == 0 ---> left click
        {
            var mousePos = currentEvent.mousePosition;
            CurrentSelection.x = Mathf.Floor((mousePos.x + ScrollPos.x) / tileSize.x);
            CurrentSelection.y = Mathf.Floor((mousePos.y + ScrollPos.y) / tileSize.y);
            if (CurrentSelection.x >= gridSize.x)
                CurrentSelection.x = gridSize.x - 1;
            if (CurrentSelection.y >= gridSize.y)
                CurrentSelection.y = gridSize.y - 1;
            selection.TileId = (int)(CurrentSelection.y * gridSize.x + CurrentSelection.x + 1);
            Repaint();
        }

        GUI.EndScrollView();
    }
}