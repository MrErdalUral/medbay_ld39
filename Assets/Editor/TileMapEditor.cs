﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

//Custom inspector for TileMap game object 
[CustomEditor(typeof(TileMap))]
public class TileMapEditor : Editor
{
    public TileMap Map;
    private TileBrush _brush;
    private Vector3 _mouseHitPosition;

    bool MouseOnMap
    {
        get
        {
            return _mouseHitPosition.x >= 0
                && _mouseHitPosition.x <= Map.GridSize.x
                && _mouseHitPosition.y >= 0
                && _mouseHitPosition.y <= Map.GridSize.y;
        }
    }
    //Called like an update method on Inspector
    public override void OnInspectorGUI()
    {
        //Keep the map size value to detect changes
        var oldMapSize = Map.MapSize;
        //Keeğ the texture value to detect changes
        var oldTexture = Map.Texture2D;

        EditorGUILayout.BeginVertical();

        //Get map size from GUI
        Map.MapSize = EditorGUILayout.Vector2Field("Map Size", Map.MapSize);


        //Get texture from GUI
        Map.Texture2D = (Texture2D)EditorGUILayout.ObjectField("Texture", Map.Texture2D, typeof(Texture2D), false);

        if (Map.Texture2D == null)
        {
            //Display warning message if no texture is selected
            EditorGUILayout.HelpBox("You have not selected a texture", MessageType.Warning);
        }
        else
        {
            //Display Map Properties
            EditorGUILayout.LabelField("Tile Size", Map.TileSize.ToString());
            EditorGUILayout.LabelField("Grid Size In Units", Map.GridSize.ToString());
            EditorGUILayout.LabelField("Pixels Per Unit", ((int)Map.PixelsPerUnit).ToString());
            //if texture is selected update brush sprite
            UpdateBrush(Map.CurrentBrushSprite);
            if(GUILayout.Button("Clear All Tiles") && EditorUtility.DisplayDialog("Clear All Tiles","Are You Sure?","Yes","No"))
                ClearAllTiles();
        }
        EditorGUILayout.EndVertical();
        //Update property values if a change has been detected
        if (oldMapSize != Map.MapSize || oldTexture != Map.Texture2D)
            UpdateMapCalculations();

    }
    //Called when the game object is selected
    void OnEnable()
    {
        Map = target as TileMap;
        Tools.current = Tool.View;

        //Create a tile game object for grouping up tiles
        if (Map.Tiles == null)
        {
            var go = new GameObject("Tiles");
            go.transform.SetParent(Map.transform);
            go.transform.position = Vector3.zero;
            Map.Tiles = go;
        }

        if (Map.Texture2D != null)
        {
            UpdateMapCalculations();
            NewBrush();
        }
    }
    //Called when the game object is unselected
    void OnDisable()
    {
        DestroyBrush();
    }
    //Called like an update method on scene
    void OnSceneGUI()
    {
        if (_brush == null) return;

        UpdateHitPosition();
        if (MouseOnMap)
        {
            MoveBrush();
            if (Map.Texture2D != null)
            {
                Event currentEvent = Event.current;
                if ((currentEvent.type == EventType.MouseDown && currentEvent.button == 0) || currentEvent.shift)
                    Draw();
                if ((currentEvent.type == EventType.MouseDown && currentEvent.button == 1) || currentEvent.control)
                    RemoveTile();
            }
        }
    }

    //This method calculates and updates necessary fields for tile TileMap object according to the selected size and texture parameters
    private void UpdateMapCalculations()
    {
        var path = AssetDatabase.GetAssetPath(Map.Texture2D);
        Map.SpriteReferences = AssetDatabase.LoadAllAssetsAtPath(path);
        var sprite = (Sprite)Map.SpriteReferences[1]; //0 index is the whole texture itself
        var width = sprite.textureRect.width;
        var height = sprite.textureRect.height;
        //Get PixelsPerUnit from sprite
        Map.PixelsPerUnit = sprite.pixelsPerUnit;
        //Get tile size in pixels
        Map.TileSize = new Vector2(width, height);
        //get grid size in units
        Map.GridSize = new Vector2(width * Map.MapSize.x / Map.PixelsPerUnit, height * Map.MapSize.y / Map.PixelsPerUnit);

        Map.TileId = 1;
    }

    void CreateBrush()
    {
        var sprite = Map.CurrentBrushSprite;
        if (sprite != null)
        {
            GameObject go = new GameObject("Brush");
            go.transform.SetParent(Map.transform);
            _brush = go.AddComponent<TileBrush>();
            _brush.Renderer2D = go.AddComponent<SpriteRenderer>();
            _brush.Renderer2D.sortingOrder = 10000;
            var pixelsPerUnit = Map.PixelsPerUnit;
            _brush.BrushSize = new Vector2(sprite.textureRect.width / pixelsPerUnit, sprite.textureRect.height / pixelsPerUnit);
            _brush.UpdateBrush(sprite);
        }
    }

    void NewBrush()
    {
        if (_brush == null)
            CreateBrush();
    }

    void DestroyBrush()
    {
        if (_brush != null)
            GameObject.DestroyImmediate(_brush.gameObject);
    }

    public void UpdateBrush(Sprite sprite)
    {
        if (_brush == null)
        {
            NewBrush();
            return;
        }
        _brush.UpdateBrush(sprite);
    }
    //This methods raycasts to find and store the mouse position. 
    void UpdateHitPosition()
    {
        var p = new Plane(Map.transform.TransformDirection(Vector3.forward), Vector3.zero);
        var ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
        var hit = Vector3.zero;
        var distance = 0f;
        if (p.Raycast(ray, out distance))
        {
            //hit = ray.origin + ray.direction.normalized * distance;
            hit = ray.GetPoint(distance);
        }
        _mouseHitPosition = Map.transform.InverseTransformDirection(hit);
    }

    //This method moves the brush to a tilemap cell corresponding to the hit position found with the UpdateHitPosition method
    void MoveBrush()
    {
        var tileSizeInUnits = Map.TileSize.x / Map.PixelsPerUnit;
        var row = Mathf.Floor(_mouseHitPosition.y / tileSizeInUnits);
        var column = Mathf.Floor(_mouseHitPosition.x / tileSizeInUnits);

        var x = column * tileSizeInUnits;
        var y = row * tileSizeInUnits;

        var id = (int)(Map.MapSize.x * row + column);
        _brush.TileId = id;

        x += Map.transform.position.x + Map.TileSize.x / (2 * Map.PixelsPerUnit);
        y += Map.transform.position.y + Map.TileSize.y / (2 * Map.PixelsPerUnit);

        _brush.transform.position = new Vector3(x, y, Map.transform.position.z);
    }

    void Draw()
    {
        var id = _brush.TileId.ToString();

        //var x = _brush.transform.position.x;
        //var y = _brush.transform.position.y;

        var tile = GameObject.Find(Map.name + "/Tiles/tile_" + id);
        if (tile == null)
        {
            tile = new GameObject("tile_" + id);
            tile.transform.SetParent(Map.Tiles.transform);
            tile.transform.position = _brush.transform.position;
            tile.AddComponent<SpriteRenderer>();
        }
        tile.GetComponent<SpriteRenderer>().sprite = _brush.Renderer2D.sprite;
    }

    void RemoveTile()
    {
        var id = _brush.TileId.ToString();
        var tile = GameObject.Find(Map.name + "/Tiles/tile_" + id);
        if(tile == null) return;
        GameObject.DestroyImmediate(tile);
    }

    void ClearAllTiles()
    {
        for (int i = 0;i<Map.Tiles.transform.childCount;)
        {
            GameObject.DestroyImmediate(Map.Tiles.transform.GetChild(i).gameObject);
        }
    }
}
