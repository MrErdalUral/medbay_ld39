﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class TileMapMenu
{
    [MenuItem("GameObject/Create Tile Map")]
    public static void CreateTileMapMenu()
    {
        //Add create tile map option to GameOject menu 
        var go = new GameObject("Tile Map");
        go.AddComponent<TileMap>();
    }
}
